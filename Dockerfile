FROM php:8.0-cli

WORKDIR /var/www

RUN apt-get update && apt-get install -y git curl zip \
  && docker-php-ext-install -j$(nproc) pdo_mysql pcntl

RUN chown -R www-data:www-data /var/www
