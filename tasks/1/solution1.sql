SELECT u.user_id, u.username, SUM(p.post_id)
FROM users u
         LEFT JOIN posts p ON u.user_id = p.user_id
         LEFT JOIN users_meta um ON u.user_id = um.user_id
         LEFT JOIN user_plan up ON u.user_id = up.user_id
WHERE um.meta_name = 'user_language'
  AND um.meta_value IN('english', 'spanish')
  AND up.plan_id = 19
  AND up.start BETWEEN NOW() - INTERVAL 1 YEAR AND NOW()
GROUP BY u.user_id
HAVING SUM(p.post_id) > 5;
