<?php

declare(strict_types=1);

require_once 'src/App.php';
require_once 'src/DBConnection.php';
require_once 'src/Import.php';
require_once 'src/LazyCsvFile.php';
require_once 'src/Output.php';

$_ENV += [
    'DB_HOST' => 'mysql',
    'DB_PORT' => 3306,
    'DB_DATABASE' => 'stage_postopl_db',
    'DB_USERNAME' => 'postoplan',
    'DB_PASSWORD' => 'secret',
];

(new Output(
    (new App(...$argv))->run()
))->print();
