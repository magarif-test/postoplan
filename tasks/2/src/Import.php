<?php

declare(strict_types=1);

class Import
{
    public function __construct(
        private string $table,
        private LazyCsvFile $file,
        private DBConnection $db
    ) { }

    public function run(): int
    {
        return $this->db->transaction(function (): int {
            $lines = 0;
            foreach ($this->file->getRows() as $row) {
                $this->push($this->prepare($row));
                ++$lines;
            }

            return $lines;
        });
    }

    private function prepare(array $row): array
    {
        // `period` format %y.%m

        // if empty `data_value` field
        $row[2] = $row[2] !== '' ? $row[2] : null;

        // `status` and `units` - enums?

        return $row;
    }

    private function push(array $row): void
    {
        static $count = 0;
        static $stack = [];

        $stack[] = $row;
        if (++$count >= $this->db::MAX_MULTIPLE_INSERT_ROWS_COUNT) {
            $this->db->multipleInsert($this->table, $this->file->getFields(), $stack);

            $count = 0;
            $stack = [];
        }
    }
}
