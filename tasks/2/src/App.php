<?php

declare(strict_types=1);

final class App
{
    private const TABLE = 'task2_table';

    public function __construct(
        string $script,
        private string $filename
    ) { }

    public function run(): array
    {
        $import = new Import(
            self::TABLE,
            new LazyCsvFile($this->filename),
            new DBConnection($_ENV)
        );

        return ['Lines' => $import->run()];
    }
}
