<?php

declare(strict_types=1);

class DBConnection
{
    public const MAX_MULTIPLE_INSERT_ROWS_COUNT = 20_000;

    private PDO $dbh;

    public function __construct(array $config)
    {
        $this->dbh = new PDO(
            sprintf(
                'mysql:host=%s;port=%d;dbname=%s',
                $config['DB_HOST'],
                $config['DB_PORT'],
                $config['DB_DATABASE']
            ),
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );
    }

    /**
     * @param string $query
     * @param array $bindings
     *
     * @return bool
     */
    public function execute(string $query, array $bindings = []): bool
    {
        return $this->dbh->prepare($query)->execute($bindings);
    }

    /**
     * @param Closure $closure
     *
     * @return mixed
     * @throws Throwable
     */
    public function transaction(Closure $closure): mixed
    {
        try {
            $this->dbh->beginTransaction();
            $result = $closure();
            $this->dbh->commit();

            return $result;
        } catch (Throwable $exception) {
            $this->dbh->rollBack();
            throw $exception;
        }
    }

    /**
     * @param string $field
     *
     * @return string
     */
    public function quote(string $field): string
    {
        return '`'. $field .'`';
    }

    /**
     * @param array $fields
     *
     * @return string
     */
    public function concat(array $fields): string
    {
        return implode(', ', $fields);
    }

    /**
     * @param string $field
     *
     * @return string
     */
    public function brackets(string $field): string
    {
        return '('. $field .')';
    }

    /**
     * @param string $table
     * @param array $fields
     * @param array $data
     *
     * @return void
     */
    public function multipleInsert(string $table, array $fields, array $data): void
    {
        $values = $placeholders = [];
        foreach ($data as $row) {
            array_push($values, ...$row);
            $placeholders[] = $this->concat(array_fill(0, count($row), '?'));
        }

        $query = sprintf(
            'INSERT INTO %s (%s) VALUES %s;',
            $this->quote($table),
            $this->concat(array_map([$this, 'quote'], $fields)),
            $this->concat(array_map([$this, 'brackets'], $placeholders)),
        );

        $this->execute($query, $values);
    }
}
