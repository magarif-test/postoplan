<?php

declare(strict_types=1);

class LazyCsvFile
{
    /** @var resource */
    private $handle;
    private array $fields;

    public function __construct(
        string $filename,
        private string $separator = ',',
        private string $enclosure = '"',
        private string $escape = '\\'
    ) {
        $this->handle = fopen($filename, 'rb');
        if ($this->handle === false) {
            throw new RuntimeException('Failed to open file for reading.');
        }

        $this->setFields($this->getNextRow() ?? []);
    }

    public function __destruct()
    {
        if ($this->handle) {
            fclose($this->handle);
        }
    }

    private function setFields(array $fields): void
    {
        $this->fields = array_map('strtolower', $fields);
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function getRows(): Iterator
    {
        while ($row = $this->getNextRow()) {
            yield $row;
        }
    }

    private function getNextRow(): ?array
    {
        return fgetcsv($this->handle, null, $this->separator, $this->enclosure, $this->escape) ?: null;
    }
}
