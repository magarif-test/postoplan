<?php

declare(strict_types=1);

class Output
{
    public function __construct(private array $info) { }

    public function print(): void
    {
        foreach ($this->info as $name => $value) {
            echo $name, ': ', $value, PHP_EOL;
        }

        echo 'Time: ', microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], PHP_EOL;
    }
}
