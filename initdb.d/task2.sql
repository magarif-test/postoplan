
DROP TABLE IF EXISTS `task2_table`;

CREATE TABLE `task2_table` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `series_reference` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    `period` float NOT NULL,
    `data_value` double NULL,
    `status` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    -- `status` enum('FINAL','PROVISIONAL','CONFIDENTIAL','REVISED') COLLATE utf8_unicode_ci NOT NULL,
    `units` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    -- `units` enum('Index','Dollars','Percent') COLLATE utf8_unicode_ci NOT NULL,
    `magntude` tinyint(1) COLLATE utf8_unicode_ci NOT NULL,
    `subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    `group` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
    `series_title_1` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `series_title_2` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `series_title_3` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `series_title_4` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `series_title_5` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
